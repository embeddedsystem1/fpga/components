library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity add is 
generic(N : integer :=8);
port (EN,RST: in std_logic;
		in1,in2: in std_logic_vector(N-1 downto 0);
		add_out: out unsigned(N-1 downto 0));
end;



architecture benhav of add is
begin
add_out<=unsigned(in1)+unsigned(in2);
end;