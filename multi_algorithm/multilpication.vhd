library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity multilpication is 
generic(N: integer :=8);
port (clk,EN,RST: in std_logic;
		x,y: in std_logic_vector(N-1 downto 0);
		done: out std_logic;
		P: out std_logic_vector(2*N-1 downto 0));
		
end;


architecture RTL of multilpication is
signal Mplier,Mcand,Mcand_wire: std_logic_vector(N-1 downto 0);
signal sum_wire_std: std_logic_vector(N-1 downto 0);
signal sum_wire: unsigned(N-1 downto 0);
signal shift_wire : std_logic_vector(2*N-1 downto 0);
signal ctrl_out_wire: std_logic_vector(3 downto 0);
signal done_pin,wr_pin,wrsh_pin: std_logic;
signal shift_pin: std_logic;
signal Q0_wire :std_logic;




component ctrl is
generic(N: integer :=8);
port(clk,EN,RST: in std_logic;
	Q0_input: in std_logic;
	ctrl_output: out std_logic_vector(3 downto 0));
end component;

component add is
	generic(N : integer :=8);
	port (
		in1,in2: in std_logic_vector(N-1 downto 0);
		add_out: out unsigned(N-1 downto 0));
end component;

component Mcand_reg is
generic(N: integer :=8);
port (clk,EN,RST: in std_logic;
		wr: in std_logic;
		reg_input: in std_logic_vector(N-1 downto 0);
		reg_output: out std_logic_vector(N-1 downto 0));
end component;

component shift_reg is

generic(N: integer :=8);
port (clk,EN,RST: in std_logic;
		shift,wr,wr_sum: in std_logic;
		reg_input: in std_logic_vector(2*N-1 downto 0);
		reg_output: out std_logic_vector(2*N-1 downto 0));
		
end component;

begin
	Mplier<=x when rising_edge(clk) and EN='1';
	Mcand<=y when rising_edge(clk) and EN='1';
	P<=shift_wire when done_pin ='1';
	
	wr_pin<=ctrl_out_wire(3);
	wrsh_pin<=ctrl_out_wire(2);
	shift_pin<=ctrl_out_wire(1);
	done_pin<=ctrl_out_wire(0);
	done <=done_pin;
	Q0_wire<=shift_wire(0);
	sum_wire_std<=std_logic_vector(sum_wire);
	
	add_unit:	add port map(shift_wire(2*N-1 downto N),Mcand_wire,sum_wire);
	Mcand_unit:	Mcand_reg port map(clk,EN,RST,wr_pin,Mcand,Mcand_wire);
	shift_unit:	shift_reg port map(clk,EN,RST,shift_pin,wr_pin,wrsh_pin,(sum_wire_std&Mplier),shift_wire);
	ctrl_unit: ctrl port map(clk,EN,RST,Q0_wire,ctrl_out_wire);


end ;

		

