library ieee;
use ieee.std_logic_1164.all;


entity Mcand_reg is
generic(N: integer :=8);
port (clk,EN,RST: in std_logic;
		wr: in std_logic;
		reg_input: in std_logic_vector(N-1 downto 0);
		reg_output: out std_logic_vector(N-1 downto 0));
end;

architecture benhav of Mcand_reg is
signal reg: std_logic_vector(N-1 downto 0);
begin
reg_output<=reg;
process(clk,EN,RST)
begin
	if RST='1' then
		reg<=(others=>'0');
	elsif clk='1' and clk'event and EN='1' then
		if wr='1' then
			reg<=reg_input;
			
		end if;
	end if;
	
end process;
end;