library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_reg is
generic(N: integer :=8);
port (clk,EN,RST: in std_logic;
		shift,clear,load,wr,wr_sum: in std_logic;
		reg_input: in std_logic_vector(2*N-1 downto 0);
		reg_output: out std_logic_vector(2*N-1 downto 0));
end;

architecture benhav of shift_reg is
signal reg: std_logic_vector(2*N-1 downto 0):=(others=>'0');
begin
process(clk,EN,RST)
begin
	if RST='1' then
		reg<=(others=>'0');
	elsif clk='1' and clk'event and EN='1' then
		if wr='1' then
			reg<=reg_input;
		elsif wr_sum='1' then
			reg(2*N-1 downto N)<=reg_input(2*N-1 downto N);
		elsif shift='1' then
			reg<='0'&reg(2*N-1 downto 1);
			
		end if;
	end if;
end process;
reg_output<=reg;
end;