library ieee;
use ieee.std_logic_1164.all;


entity adder is 
generic(n:natural :=8);
port (in1,in2: in std_logic_vector (n-1 downto 0);
		EN: in std_logic;
		sum_out: out std_logic_vector(n downto 0);
		cin: in std_logic);
end;

architecture rtl of adder is
signal carry: std_logic_vector(n-1 downto 0);
signal sum: std_logic_vector(n downto 0);
component fulladd
	port (a,b,cin: in std_logic;
	--	add_EN: in std_logic;
		s,c: out std_logic);
end component;
begin
gen: for i in 1 to n-1 generate
	u: fulladd port map(in1(i),in2(i),carry(i-1),sum(i),carry(i));
	end generate;
u0: fulladd port map(in1(0),in2(0),cin,sum(0),carry(0));
sum(n)<=carry(n-1);
sum_out<=sum when EN='1';
end;  	 