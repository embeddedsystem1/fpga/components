library ieee;
use ieee.std_logic_1164.all;


entity fulladd is
port (a,b,cin: in std_logic;
		add_EN:in std_logic;
		s,c: out std_logic);
end;


architecture rtl of fulladd is
signal D1,N1,N2,c_wire,s_wire: std_logic;
begin
	
	D1<=a xor b;
	s_wire<=D1 xor cin;
	N1<=D1 and cin;
	N2<=a and b;
	c_wire<= N1 or N2;
	s<=s_wire;
	c<=c_wire;
end;