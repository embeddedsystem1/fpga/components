library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ctrl is
generic(N: integer :=8);
port(clk,EN,RST: in std_logic;
	Q0_input: in std_logic;
	ctrl_output: out std_logic_vector(3 downto 0));
end;

architecture states of ctrl is
type state is (start_state,Q0_state,shift_state,done_state);
signal current_state: state:= start_state;

begin

process(clk,EN,RST)
variable ctrl_reg: std_logic_vector(3 downto 0):=b"1100";-- wr_Mcand/wr_sh/shift/done 
variable count: unsigned(3 downto 0):=to_unsigned(0,4);
begin
if RST='1' then 
	current_state<=start_state;
	count:=to_unsigned(0,4);
	ctrl_reg:=b"1100";
elsif clk='1' and clk'event and EN='1' then
	case current_state is
		when start_state =>
			ctrl_reg:=b"1100";
			count:=to_unsigned(0,4);
			current_state<=Q0_state;
		when Q0_state =>
				if Q0_input='1' then
					ctrl_reg:=b"0100";
				elsif Q0_input='0' then
					ctrl_reg:=b"0000";
				end if;
				current_state<=shift_state;
			
		when shift_state=>
			
			if count<9 then
				ctrl_reg:=b"0010";
				current_state<=Q0_state;
			else 
				current_state<=done_state;
			end if;
			count:=count+1;
		when done_state =>
			ctrl_reg:=b"0001";
			current_state<=start_state;
	end case;
	
end if;
ctrl_output<=ctrl_reg;
end process;

end;