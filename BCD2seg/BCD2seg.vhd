library ieee;
use ieee.std_logic_1164.all;


entity BCD2seg is
generic(N : integer:= 8;
		N2: integer := 4);
port (Xin: in std_logic_vector(N-1 downto 0);
	out1,out2: out std_logic_vector(N-2 downto 0);
	clk,EN: in std_logic);
end;

architecture benhav of BCD2seg is

begin
seg1: process(clk,EN,Xin)
	variable x:std_logic_vector(N2-1 downto 0);
begin
	--out1<="11111111";
	x:=Xin(N2-1 downto 0);
	if clk='1' and clk'event and EN='1' then
	
	case x is 
		when b"0000"=>out1<="1000000";
		when b"0001"=>out1<="1111001";
		when b"0010"=>out1<="0100100";
		when b"0011"=>out1<="0110000";
		when b"0100"=>out1<="0011001";
		when b"0101"=>out1<="0010010";
		when b"0110"=>out1<="0000010";
		when b"0111"=>out1<="1111000";
		when b"1000"=>out1<="0000000";
		when b"1001"=>out1<="0010000";
		when b"1010"=>out1<="0001000";
		when b"1011"=>out1<="0000011";
		when b"1100"=>out1<="1000110";
		when b"1101"=>out1<="0100001";
		when b"1110"=>out1<="0000110";
		when b"1111"=>out1<="0001110";
	end case;
	end if;
end process;
seg2: process(clk,EN,Xin)
	variable x:std_logic_vector(N2-1 downto 0);
begin
	--out2<="11111111";
	x:=Xin(N-1 downto N2);
	if clk='1' and clk'event and EN='1' then
	case x is 
		when b"0000"=>out2<="1000000";
		when b"0001"=>out2<="1111001";
		when b"0010"=>out2<="0100100";
		when b"0011"=>out2<="0110000";
		when b"0100"=>out2<="0011001";
		when b"0101"=>out2<="0010010";
		when b"0110"=>out2<="0000010";
		when b"0111"=>out2<="1111000";
		when b"1000"=>out2<="0000000";
		when b"1001"=>out2<="0010000";
		when b"1010"=>out2<="0001000";
		when b"1011"=>out2<="0000011";
		when b"1100"=>out2<="1000110";
		when b"1101"=>out2<="0100001";
		when b"1110"=>out2<="0000110";
		when b"1111"=>out2<="0001110";
	end case;
	end if;
end process;
end;