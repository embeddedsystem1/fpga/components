library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned;
use ieee.numeric_std.all;



entity count is 
port (clk ,EN_count:in std_logic;
		out_count: out std_logic);
end ;

architecture benha of count is
begin
process(clk ,EN_count)
		variable reg_count : integer range 0 to 20;
begin
	if clk='1' and clk'event then
		if EN_count ='0' then
		reg_count:=reg_count+1;
		if reg_count=9	then
			out_count<='1';
		end if ;
		else 
		reg_count :=0;
		out_count<='0';
		end if;
	end if;
end process;
end;
		
		