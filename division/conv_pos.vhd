library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


entity conv_pos is 
port (in_conv : in std_logic_vector(7 downto 0);
		out_conv : out std_logic_vector(7 downto 0);
		PN : out std_logic;
		write_conv:in std_logic);
end ;


architecture benha of conv_pos is 
--type reg is array(7 downto 0) of std_logic;
signal reg_in :std_logic_vector(7 downto 0);
--signal reg_out: reg;

begin
	reg_in<=in_conv;
process(in_conv)
variable reg_pos: std_logic_vector (7 downto 0);
variable reg_pn : std_logic;
begin
 if write_conv ='1' then
	if reg_in(7) = '0' then
		reg_pos:=in_conv;
		out_conv<=reg_pos;
		reg_pn:='0';
		pn<=reg_pn;
	else 
		reg_pos:=(x"ff"-in_conv)+1;
		out_conv<=reg_pos;
		reg_pn:='1';
		pn<=reg_pn;
end if ;

else
pn<=reg_pn;
out_conv<=reg_pos;
end if; 

end process;
end;