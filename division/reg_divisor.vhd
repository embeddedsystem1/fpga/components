library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;



entity reg_divisor is
port (in_divisor : in std_logic_vector(7 downto 0);
		out_divisor :out std_logic_vector(7 downto 0);
		clk,write_dvsr: in std_logic);
end;
architecture rtl of reg_divisor is 
--signal reg_divisor : std_logic_vector(7 downto 0);
begin
process(clk)
variable reg :std_logic_vector(7 downto 0);
begin
	if clk='1' and clk'event then
	if write_dvsr='1' then
		reg:=in_divisor;
	end if;
	
		out_divisor<=reg;
	
	end if;
end process;
		
end;
