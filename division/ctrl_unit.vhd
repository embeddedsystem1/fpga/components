library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;



entity ctrl_unit is
port (write_dvnd,count,r,pn_dvsr,pn_dvnd:in std_logic;
		EN,shiftL,quo,write_rem,neg_rem,neg_quo:out std_logic);
		
end;

architecture RTL of ctrl_unit is
signal y,q :std_logic;
begin
	
	y<=write_dvnd nor count;
	EN<=y;
	shiftL<=y and (not write_dvnd);
	q<=y and r;
	quo<=q;
	write_rem <= q;
	neg_rem<=pn_dvnd;
	neg_quo<=pn_dvnd xor pn_dvsr;
end;