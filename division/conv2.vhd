library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;



entity conv2 is 
port (in_conv: in std_logic_vector(15 downto 0);
		out_conv: out std_logic_vector(15 downto 0);
		neg_rem,neg_quo:in std_logic);
end ;

architecture  benha of conv2 is 

begin
	process(in_conv,neg_rem,neg_quo)
	variable reg_quo,reg_rem:std_logic_vector(7 downto 0);
	begin
		if neg_quo ='1' then
		reg_quo:=not(in_conv(7 downto 0))+'1';
		else
		reg_quo:= in_conv(7 downto 0);
		end if;
		if neg_rem='1' then
		reg_rem:=not(in_conv(15 downto 8))+'1';
		else
		reg_rem:= in_conv(15 downto 8);
		end if;
		out_conv<= reg_rem & reg_quo;
	end process;
end ;