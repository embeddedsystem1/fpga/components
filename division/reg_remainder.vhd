library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


entity reg_remainder is 
port(clk,quo_bit,shiftL,write_dvnd,write_rem,EN_rem :in std_logic;
		in_rem,in_dvnd : in std_logic_vector(7 downto 0);
		out_rem : out std_logic_vector(7 downto 0);
		out_rem2 : out std_logic_vector(15 downto 0));
end ;

architecture benha of reg_remainder is
signal reg : std_logic_vector(15 downto 0 ):=x"0000";
begin

process(shiftL,write_dvnd,write_rem,EN_rem )
		variable reg_next,reg_rem:std_logic_vector(15 downto 0):=x"0000";
		variable reg_q :std_logic_vector(7 downto 0):=X"00";
	begin
		if clk='1' and clk'event then
		if write_dvnd = '1' then 
			reg_next(7 downto 0):=in_dvnd;
		end if;
		if EN_rem ='1' then
		if write_rem='1' then
			reg_next(15 downto 8 ):= in_rem;
		end if;
		
		 if shiftL ='1'then 
			reg_rem:=reg_next;
			reg_next:=reg_next(14 downto 0)&'0';
			reg_q:=reg_q(6 downto 0)&quo_bit;
		--	for i in 14 downto 0 loop
			
			
		end if;
		
		end if;
		end if;
		reg<=reg_next;
		out_rem2<=reg_rem(15 downto 8)&reg_q;
		out_rem<=reg_next(15 downto 8);
	end process;
end;
			
			
		
