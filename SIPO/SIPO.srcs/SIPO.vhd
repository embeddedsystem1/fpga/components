
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SIPO is
    generic(
    data_width : integer:=8 --default 8 bits
     );
    Port (
    		srclk : in STD_LOGIC; 		--clock input
    		en: in STD_LOGIC;  			--enable pin
           	oe_not : in STD_LOGIC;		--output enable(low active)
          	srclr_not : in STD_LOGIC;	--clear srial (low active)
          	ser_input : in STD_LOGIC;	--serial input
           	load: in STD_LOGIC;			--load pin
           	q : out STD_LOGIC_VECTOR (data_width-1 downto 0);	--parallel output
            ser_out :out STD_LOGIC 		--serial output
           );
end SIPO;

architecture RTL of SIPO is
signal q_ser_signal: STD_LOGIC_VECTOR(data_width-1 downto 0 );
signal q_ser_not_signal : STD_LOGIC_VECTOR(data_width-1 downto 0 );
signal q_signal: STD_LOGIC_VECTOR(data_width-1 downto 0 );
signal dffe3_en : STD_LOGIC;

component dffe
	port ( clk : in STD_LOGIC;
          en : in STD_LOGIC;
          rst_not : in STD_LOGIC;
          d : in STD_LOGIC;
          q : out STD_LOGIC;
          q_not : out STD_LOGIC);
end component;

begin
	dffe3_en<= not(oe_not) and load; --load serial data on q
	
	--generate  two columns of D latchs  to get and shift serial data and load on parallel output 
	dffe_generate :for k in 0 to data_width-1  generate
	
		--the first D latch in the first column to get serial data
		dffe1_generate: if k=0 generate
			dffe1: dffe port map(clk=>srclk, en=>en, rst_not => srclr_not, d=>ser_input, q=>q_ser_signal(k));  
		end generate;
		
		-- generate the first column of  D latchs to shift srial data
		dffe2_generate: if k>=1 generate
			dffe2: dffe port map(clk=>srclk, en=>en, rst_not => srclr_not, d=>q_ser_signal(k-1), q=>q_ser_signal(k));
		end generate;	
		
		--generat the second column of D latch to load serial data on q pins 
		dffe3: dffe port map(clk=>srclk, en=>dffe3_en, rst_not => srclr_not, d=>q_ser_signal(k),q=>q(k));
		
	end generate;
	
	ser_out<=q_ser_signal(data_width-1); --serial output 
  
end RTL;
