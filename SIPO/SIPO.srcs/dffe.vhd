----------------------------------------------------------------------------------
-- Module Name: dffe - RTL
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity dffe is
    Port ( clk : in STD_LOGIC;
           en : in STD_LOGIC;
           rst_not : in STD_LOGIC;
           d : in STD_LOGIC;
           q : out STD_LOGIC;
           q_not : out STD_LOGIC);
end dffe;

architecture RTL of dffe is
    signal q_signal : std_logic;
begin
	q<=q_signal;
	q_not<=not(q_signal);
    dffe:process(clk,rst_not,en)
    begin
    	if rst_not='0' then
    		q_signal<='0'; --reset output
        elsif rising_edge(clk) and en='1'  then
            q_signal<=d; --latch data
        end if;
    end process;

end RTL;
