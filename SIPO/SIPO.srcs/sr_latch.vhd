
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity sr_latch is
    Port ( clk : in STD_LOGIC;
           en : in STD_LOGIC;
           rst_not : in STD_LOGIC;
           r : in STD_LOGIC;
           s : in STD_LOGIC;
           q : out STD_LOGIC;
           q_not : out STD_LOGIC);
end sr_latch;

architecture RTL of sr_latch is
	signal q_signal: STD_LOGIC;
	signal q_not_signal: STD_LOGIC;
begin
	--RTL of SR latch
	q_signal<=q_not_signal nand r;
	q_not_signal<= q_signal nand s;
	--conditional output
	clock:process(clk,en,rst_not)
	begin
		if rst_not='0' then
			q<='0'; 
		elsif rising_edge(clk) and en='1' then
			q<=q_signal;
			q_not<=q_not_signal;
		end if;
	end process;
end RTL;
